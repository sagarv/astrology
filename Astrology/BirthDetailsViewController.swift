//
//  BirthDetailsViewController.swift
//  Astrology
//
//  Created by Rishabh Wadhwa on 05/12/15.
//  Copyright © 2015 Sunpac Solutions. All rights reserved.
//

import UIKit
import CoreData

class BirthDetailsViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    //To get all the product details
    let dataController = DataController.sharedInstance
    
    @IBOutlet weak var birthDate: UITextField!
    @IBOutlet weak var birthTime: UITextField!
    @IBOutlet weak var birthPlace: UITextField!
    @IBOutlet weak var nextButtonView: UIButton!
    @IBOutlet var viewContainer: UIView!
    
    var birthDateText:String!
    var birthTimeText:String!
    var birthPlaceText:String!
    
    // Retreive the managedObjectContext from AppDelegate
    let managedContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    
    // innitialise all picker views
    let datePicker:UIDatePicker = UIDatePicker()
    let timePicker:UIDatePicker = UIDatePicker()
    let placePicker:UIPickerView = UIPickerView()
    
    // ########################################
    //          Picker View Functions
    // ########################################
    // returns the number of 'columns' to display.
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int{
        return 1
    }
    
    // returns the # of rows in each component..
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return dataController.placesIndia.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        print("Row: \(row)")
        return dataController.placesIndia[row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        birthPlace.text = dataController.placesIndia[row]
//        placePicker.hidden = true;
    }

    // ########################################
    //          Picker View Functions
    // ########################################
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeTextBox(birthDate, placeHolderText: "Enter Birth Date")
        customizeTextBox(birthTime, placeHolderText: "Enter Birth Time")
        customizeTextBox(birthPlace, placeHolderText: "Enter Birth Place")
        customizeNextButton()
//        customizeDateTimePicker()
        
        datePicker.datePickerMode = UIDatePickerMode.Date
        datePicker.addTarget(self, action: Selector("datePickerValueChanged:"), forControlEvents: UIControlEvents.ValueChanged)
        datePicker.maximumDate = NSDate()
        birthDate.inputView = datePicker
        
        timePicker.datePickerMode = UIDatePickerMode.Time
        timePicker.addTarget(self, action: Selector("datePickerValueChanged:"), forControlEvents: UIControlEvents.ValueChanged)
        birthTime.inputView = timePicker
        
//        placePicker.hidden = false
        placePicker.delegate = self
        placePicker.dataSource = self
        birthPlace.text = dataController.placesIndia[0]
        birthPlace.inputView = placePicker
        
        //startObservingKeyboardEvents()
        
        //Looks for single or multiple taps and dismisses keyboard and date time pickers
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        view.addGestureRecognizer(tap)
        
        //Check if birth details are already there
        fetchBirthDetails()
    }
    
    override func viewDidAppear(animated: Bool) {
        //Start observer for keyboard events
        //startObservingKeyboardEvents()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Function to know when clicked away from text field
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        if(textField == birthDate) {
            print("birthDate should end editing method called")
//            move view up according to keyboard height
//            self.view.frame = CGRectMake(self.view.frame.minX, self.view.frame.minY-datePicker.frame.height, self.view.frame.width, self.view.frame.height)
            return true
        }
        else if(textField == birthTime) {
            print("birthTime should end editing method called")
//            move view up according to keyboard height
//            self.view.frame = CGRectMake(self.view.frame.minX, self.view.frame.minY-timePicker.frame.height, self.view.frame.width, self.view.frame.height)
            return true
        }
            //if textField is birthplace
        else {
            print("birthPlace should end editing method called")
            //move view up according to keyboard height
//            self.view.frame = CGRectMake(self.view.frame.minX, self.view.frame.minY-datePicker.frame.height, self.view.frame.width, self.view.frame.height)
            
//            placePicker.hidden = false
            return true
        }
    }
    
    //Function to know when clicked away from text field
    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
        if(textField == birthDate) {
            print("birthDate should end editing method called")
            self.view.endEditing(true)
            //move view down according to keyboard height
//            self.view.frame = CGRectMake(self.view.frame.minX, self.view.frame.minY+datePicker.frame.height, self.view.frame.width, self.view.frame.height)
            setDate()
            return true
        }
        else if(textField == birthTime) {
            print("birthTime should end editing method called")
            self.view.endEditing(true)
            //move view down according to keyboard height
//            self.view.frame = CGRectMake(self.view.frame.minX, self.view.frame.minY+timePicker.frame.height, self.view.frame.width, self.view.frame.height)
            setTime()
            return true
        }
        //if textField is birthplace
        else {
            print("birthPlace should end editing method called")
            self.view.endEditing(true)
            //move view down according to keyboard height
//            self.view.frame = CGRectMake(self.view.frame.minX, self.view.frame.minY+datePicker.frame.height, self.view.frame.width, self.view.frame.height)
            birthPlaceText = birthPlace.text
            return true
        }
    }
    
    //Change status bar text to white
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }
    
    //MARK: - Customized functions
    
    //Calls this function when the tap is recognized.
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        self.view.endEditing(true)
    }
    
    func datePickerValueChanged(sender: UIDatePicker) {
        if(sender == datePicker) {
            setDate()
        }
        else {
            setTime()
        }
    }
    
    //Update birthDate text field when datepicker value changes
    func setDate() {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        birthDateText = dateFormatter.stringFromDate(datePicker.date)
        birthDate.text = birthDateText
    }
    
    //Update birthTime text field when datepicker value changes
    func setTime() {
        let timeFormatter = NSDateFormatter()
        timeFormatter.dateFormat = "HH:mm"
        birthTimeText = timeFormatter.stringFromDate(timePicker.date)
        birthTime.text = birthTimeText
    }
    
    //Set text box's border radius, color and placeholder text
    func customizeTextBox(sender: UITextField, placeHolderText: String) {
        sender.layer.cornerRadius = 25
        sender.layer.masksToBounds = true
        let placeholder = NSAttributedString(string: placeHolderText, attributes: [NSForegroundColorAttributeName: UIColor.whiteColor()])
        sender.attributedPlaceholder = placeholder
        sender.backgroundColor = UIColor(white: 0.0, alpha: 0.3)
    }
    
    //Set next buttons border radius
    func customizeNextButton() {
        nextButtonView.layer.cornerRadius = 25
        nextButtonView.layer.masksToBounds = true
        nextButtonView.backgroundColor = UIColor(white: 1.0, alpha: 0.7)
    }
    
    //Action for next button
    @IBAction func nextButton(sender: AnyObject) {
        print("Next Button Clicked")
        print("Birth date is \(birthDateText). Birth time is \(birthTimeText). Birth Place is \(birthPlaceText)")
        if(birthDateText == nil) {
            let alert = UIAlertController(title: "Alert", message: "Please enter the birth date", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
        else if(birthTimeText == nil) {
            let alert = UIAlertController(title: "Alert", message: "Please enter the birth time", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
        else if(birthPlaceText == nil || birthPlaceText == "Select Place") {
            let alert = UIAlertController(title: "Alert", message: "Please enter the birth place", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
        else {
            performSegueWithIdentifier("birthDetailsToHomeNext", sender: self)
            
            saveBirthDetails(birthDateText, birthTime: birthTimeText, birthPlace: birthPlaceText)
            dataController.dateChanged = true
        }
    }
    
    @IBAction func skipButtonAction(sender: AnyObject) {
        performSegueWithIdentifier("birthDetailsToHomeSkip", sender: self)
    }
    
    func saveBirthDetails(birthDate: String, birthTime: String, birthPlace: String) {
        let fetchRequest = NSFetchRequest(entityName: "BirthDetails")
        
        //Create new entity to save details
        let newItem = NSEntityDescription.insertNewObjectForEntityForName("BirthDetails", inManagedObjectContext: self.managedContext) as! BirthDetails
        
        //Save details here
        newItem.birthDate = birthDate
        newItem.birthTime = birthTime
        newItem.birthPlace = birthPlace
        
        //Delete all previous records
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        let myPersistentStoreCoordinator = managedContext.persistentStoreCoordinator
        
        do {
            try myPersistentStoreCoordinator!.executeRequest(deleteRequest, withContext: managedContext)
        }
        catch let error as NSError {
            print("Error deleting data \(error)")
        }
        
        do {
            try managedContext.save()
        }
        catch let error as NSError {
            print("Could not save \(error)")
        }
        fetchBirthDetails()
    }
    
    func fetchBirthDetails() {
        let fetchRequest = NSFetchRequest(entityName: "BirthDetails")
        
        //Fetch the birth details
        do {
            let results = try self.managedContext.executeFetchRequest(fetchRequest)
            print("Birth details fetched are \(results)")
            let birthDetail = results as! [BirthDetails]
            
            if(birthDetail != []) {
                dataController.birthDate = birthDetail[0].birthDate!
                dataController.birthTime = birthDetail[0].birthTime!
                dataController.birthPlace = birthDetail[0].birthPlace!
            }
            
            print("Birth details fetched are, Birth date \(dataController.birthDate), Birth time \(dataController.birthTime), Birth Place \(dataController.birthPlace)")
            if (dataController.birthDate != nil && dataController.birthTime != nil && dataController.birthPlace != nil) {
                print("Data found")
                birthDateText = dataController.birthDate
                birthTimeText = dataController.birthTime
                birthPlaceText = dataController.birthPlace
                birthDate.text = dataController.birthDate
                birthTime.text = dataController.birthTime
                birthPlace.text = dataController.birthPlace
            }
            else {
                print("No data found")
            }
        }
            
        catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
            //performSegueWithIdentifier("dataToBirthDetails", sender: self)
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        if(segue.identifier == "birthDetailsToHomeNext") {
            //let nav = segue.destinationViewController as! MenuViewController
            // Pass the selected object to the new view controller.
            print ("Prepare for Segue")
        }
        if(segue.identifier == "birthDetailsToHomeSkip") {
            //let nav = segue.destinationViewController as! MenuViewController
            // Pass the selected object to the new view controller.
            print ("Already got details. Skipping to home screen directly.")
        }
    }
}
