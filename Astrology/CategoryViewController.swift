//
//  CategoryViewController.swift
//  Astrology
//
//  Created by Rishabh Wadhwa on 26/11/15.
//  Copyright © 2015 Sunpac Solutions. All rights reserved.
//

import UIKit

class CategoryViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var categoryTab: UICollectionView!
    @IBOutlet weak var productList: UICollectionView!
    @IBOutlet weak var headerMenu: UIView!
    
    //To get all the product details
    let dataController = DataController.sharedInstance
    
    var selectedCategory:Int!
    var selectedProduct:Int!
    var productCount:Int!
    var productIDList:[Int]!
    var bounds = UIScreen.mainScreen().bounds
    
    //Set no of categories
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(collectionView == categoryTab)
        {
            return dataController.categories.count
        }
        else
        {
            return productCount
        }
    }
    
    //To set the size of cell according to screen
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize{

        let screenWidth = bounds.width
        print("Screen Width is \(screenWidth)")
        
        if(collectionView == categoryTab)
        {
            return CGSizeMake(screenWidth / 3, 50)
        }
        let margin:CGFloat! = 5
        let cellWidth = (screenWidth - (margin*4)) / 2
        let cellHeight = cellWidth * 1.5
        return CGSizeMake(cellWidth, cellHeight)
    }
    
    //Set button of each category
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        //For header category tabs
        if(collectionView == categoryTab)
        {
            let categoryCell = collectionView.dequeueReusableCellWithReuseIdentifier("categoryTab", forIndexPath: indexPath)
            
            //Get screen width
            let screenWidth = bounds.size.width
            
            //Set product title
            setCellTitle(categoryCell, tagNo: 1, cellTitle: dataController.categories[indexPath.row])
            
            let selectionPointer = categoryCell.viewWithTag(2)

            //Show tab pointer below selected category only
            if(indexPath.row == selectedCategory) {
                //by default pointer is hidden. So show it for selected category
                selectionPointer!.hidden = false
            }
            
            //Create button and set it as cell
            let categoryButton = createCellButton(screenWidth/3, cellHeight: 50, tagValue: indexPath.row, functionName: "selectCategory:")
            //Add category button to the cell
            categoryCell.addSubview(categoryButton)
     
            return categoryCell
        }
        //For product list according to category selected
        else
        {
            let productCell = collectionView.dequeueReusableCellWithReuseIdentifier("productCell", forIndexPath: indexPath)
            let screenWidth = bounds.width
            let cellWidth = (screenWidth - 20) / 2
            let cellHeight = cellWidth * 1.5
            
            productCell.layer.cornerRadius = 5
            productCell.layer.shadowColor = UIColor(white: 0x000000, alpha: 1.0).CGColor
            productCell.layer.shadowOffset = CGSizeMake(0, 1)
            productCell.layer.shadowOpacity = 1.0
            productCell.layer.shadowRadius = 0.0
            productCell.layer.masksToBounds = false
            
            //Set product title
            setCellTitle(productCell, tagNo: 2, cellTitle: dataController.productName[productIDList[indexPath.row]])
            
            //Set product image
            setProductImage(indexPath.row, productCell: productCell)
            
            //Set product price
            setProductPrice(indexPath.row, productCell: productCell)
            
            //Calculate discounts
            setProductDiscount(indexPath.row, productCell: productCell)
            
            //Create button and set it as cell
            let productBlock = createCellButton(cellWidth, cellHeight: cellHeight-1, tagValue: productIDList[indexPath.row], functionName: "selectProduct:")
            
            //Add product button to the cell
            productCell.addSubview(productBlock)
            
            return productCell
        }
    }
    
    //Set cell title
    func setCellTitle(cellName:UICollectionViewCell, tagNo:Int, cellTitle:String) {
        //Create product title
        let cellTitleLabel:UILabel! = cellName.viewWithTag(tagNo) as! UILabel
        cellTitleLabel.text = cellTitle
    }
    
    //Set product image and title
    func setProductImage(productId:Int, productCell: UICollectionViewCell) {
        //Set product image
        let productImage:UIImageView! = productCell.viewWithTag(1) as! UIImageView
        productImage.image = UIImage(named: dataController.productImageName[productIDList[productId]])
    }
    
    //Set product price and discounted price according to product id
    func setProductPrice(productId:Int, productCell: UICollectionViewCell) {
        //Set product price
        let productPrice:UILabel = productCell.viewWithTag(3) as! UILabel
        let attrString = NSAttributedString(string: dataController.currency + "\(dataController.productOldPrice[productIDList[productId]])", attributes: [NSStrikethroughStyleAttributeName: NSUnderlineStyle.StyleSingle.rawValue])
        productPrice.attributedText = attrString
        
        //Set product discounted price
        let productDiscountedPrice:UILabel! = productCell.viewWithTag(4) as! UILabel
        productDiscountedPrice.text = dataController.currency + "\(dataController.productNewPrice[productIDList[productId]])"
    }
    
    //Calculate and set product discounts
    func setProductDiscount(productId:Int, productCell: UICollectionViewCell) {
        let discountLabel:UILabel! = productCell.viewWithTag(5) as! UILabel
        let discount:Int! = Int(((CGFloat(dataController.productNewPrice[productIDList[productId]])-CGFloat(dataController.productOldPrice[productIDList[productId]]))/CGFloat(dataController.productOldPrice[productIDList[productId]]))*CGFloat(100))
        discountLabel.text = "\(discount)" + "%"
        discountLabel.layer.cornerRadius = 2
        discountLabel.layer.masksToBounds = true
    }
    
    //Create button and set it as cell
    func createCellButton(cellWidth:CGFloat, cellHeight:CGFloat, tagValue: Int, functionName: String)->UIButton {
        let cellBlock = UIButton(frame: CGRectMake(0, 0, cellWidth, cellHeight))
        
        //To pass value of product id as parameter in selectProduct Function
        cellBlock.tag = tagValue
        //To add function to the button. To open the product detail page by passing product ID
        cellBlock.addTarget(self, action: Selector(functionName), forControlEvents: .TouchUpInside)
        return cellBlock
    }
    
    func selectCategory(sender: UIButton!)
    {
        selectedCategory = sender.tag
        performSegueWithIdentifier("chooseCategory", sender: self)
    }
    
    func selectProduct(sender: UIButton!)
    {
        selectedProduct = sender.tag
        performSegueWithIdentifier("categoryToProducts", sender: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let buyViewCont = BuyViewController()
        productCount = buyViewCont.countProductsInCategory(selectedCategory)
        productIDList = buyViewCont.addToProductList(selectedCategory, productCount: productCount)
        
        //Add shadow to main header
        addShadowToHeader()
    }
    
    override func viewDidAppear(animated: Bool) {
        //To set the size of tabs and get the selected tab on the right of the screen
        self.categoryTab.frame = CGRectMake(0, self.categoryTab.frame.origin.y , self.view.frame.size.width, self.categoryTab.frame.size.height)
        self.categoryTab.scrollToItemAtIndexPath(NSIndexPath(forItem: selectedCategory, inSection: 0), atScrollPosition: .Right, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    //To open new views
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        // set new category when clicked to change category
        if (segue.identifier == "chooseCategory") {
            let nav = segue.destinationViewController as! UINavigationController
            let svc = nav.topViewController as! CategoryViewController;
            svc.selectedCategory = selectedCategory
        }
        // set first screen of Menu view controller as buy when clicked on buy
        if (segue.identifier == "categoryToBuyClick") {
            let nav = segue.destinationViewController as! MenuViewController
            nav.firstScreen = "buy"
        }
        if (segue.identifier == "categoryToProducts") {
            let nav = segue.destinationViewController as! ProductViewController
            nav.selectedProduct = selectedProduct
            nav.selectedCategory = selectedCategory
            nav.previousPage = "category"
        }
    }

    //Change status bar text to white
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }
    
    //Add shadow to main header
    func addShadowToHeader() {
        self.headerMenu.layer.shadowColor = UIColor(white: 0.3, alpha: 1.0).CGColor
        self.headerMenu.layer.shadowOffset = CGSizeMake(0, 1)
        self.headerMenu.layer.shadowOpacity = 0.7
        self.headerMenu.layer.shadowRadius = 0.3
        self.headerMenu.layer.masksToBounds = false
    }
}
