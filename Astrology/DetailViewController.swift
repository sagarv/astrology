//
//  DetailView.swift
//  Astrology
//
//  Created by Ramit Wadhwa on 28/01/16.
//  Copyright © 2016 Sunpac Solutions. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController{
    
    
    @IBOutlet var mainImage: UIImageView!
    
    @IBOutlet var mainName: UILabel!
    
    @IBOutlet var mainText: UITextView!
    
    let dataController = DataController.sharedInstance
    
    // 1 is home
    // 2 is problems
    let menu = [
        "home",
        "problems"
    ]
    
    var cameFrom = Int()
    
    var mainImageName = String()
    var mainNameText = String()
    var mainTextText = String()
    
    
    override func viewDidLoad() {
        print("Details Page Loaded")
        
        print("This page came from: \(cameFrom). Check comments for more info")
        mainImage.image = UIImage(named: mainImageName)
        mainName.text = mainNameText
        mainText.text = mainTextText
    }
    
    override func viewDidAppear(animated: Bool) {
        //Scroll to top of text
        //Animated
        mainText.scrollRangeToVisible(NSRange(location:0, length:0))
        //Non-Animated
        //mainText.scrollRectToVisible(CGRectMake(0, 0, 1, 1), animated: false)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // segue to home
        
        print("Got Segue: \(segue.identifier)")
        
        if(segue.identifier == "goBack"){

            let menuView = segue.destinationViewController as! MenuViewController
            
            menuView.firstScreen = menu[cameFrom - 1]
            
        }
        
        
        
    }
    
    //Change status bar text to white
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }
    
    //Function to call the number on click
    @IBAction func callNumber(sender: AnyObject) {
        let phoneNumber = "01149222555"
        if let phoneCallURL:NSURL = NSURL(string: "tel://\(phoneNumber)") {
            let application:UIApplication = UIApplication.sharedApplication()
            if (application.canOpenURL(phoneCallURL)) {
                application.openURL(phoneCallURL);
            }
        }
    }
    
    //Function to share details on click
    @IBAction func shareAction(sender: AnyObject) {
        let textToShare = mainNameText + "\r\n" + mainTextText
        
        //To be used to add a url if needed
        //if let myWebsite = NSURL(string: "http://www.suntist.com/")
        //{
        //  let objectsToShare = [textToShare, myWebsite]
        //Add content to this array to share
        let objectsToShare = [textToShare]
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        
        //New Excluded Activities Code
        activityVC.excludedActivityTypes = [UIActivityTypeAirDrop, UIActivityTypeAddToReadingList]
        //
        
        self.presentViewController(activityVC, animated: true, completion: nil)
        //}
    }
    
    
}
