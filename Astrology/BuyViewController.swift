//
//  BuyViewController.swift
//  Astrology
//
//  Created by Rishabh Wadhwa on 24/11/15.
//  Copyright © 2015 Sunpac Solutions. All rights reserved.
//

import UIKit

class BuyViewController: UIViewController, UIScrollViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var productList: UITableView!
    
    //To get all the product details
    let dataController = DataController.sharedInstance
    
    //To pass the value of selected category
    var selectedCategory:Int!
    var selectedProduct:Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //To set the parameters of the sliding banner
        self.scrollView.frame = CGRectMake(0, 0, self.view.frame.width, 125)
        
        //To set all the images of the sliding banner
        setBannerImages()
        
        //To set the overall size of scroller
        self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.width * 3, self.scrollView.frame.height)
        self.scrollView.delegate = self
        self.pageControl.currentPage = 0
        
        //To auto slide the banner
        NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: "moveToNextPage", userInfo: nil, repeats: true)
    }
    
    //Function to count the products in current category
    func countProductsInCategory(categoryID: Int) -> Int! {
        var productCount: Int = 0
        dataController.productsCategory.forEach { element in
            print("Current element is \(element)")
            if(element == categoryID) {
                productCount += 1
            }
        }
        return productCount
    }
    
    //Function to make a list of products in current category
    func addToProductList(categoryID: Int, productCount: Int) -> [Int]! {
        var productList: [Int] = []
        for(var i=0; i<dataController.productsCategory.count; i++) {
            print("Products Category from list is \(dataController.productsCategory[i]) and category ID is \(categoryID)")
            if(dataController.productsCategory[i] == categoryID) {
                productList.append(i)
            }
        }
        print("Product list in current category is \(productList)")
        return productList
    }
    
    //MARK: - Customized functions
    
    //To set all the images of the sliding banner
    func setBannerImages() {
        let scrollViewWidth:CGFloat = self.scrollView.frame.width
        let imgOne = UIImageView(frame: CGRectMake(0, 0,scrollViewWidth, 125))
        imgOne.image = UIImage(named: "bannerImage")
        let imgTwo = UIImageView(frame: CGRectMake(scrollViewWidth, 0,scrollViewWidth, 125))
        imgTwo.image = UIImage(named: "careerProblems")
        let imgThree = UIImageView(frame: CGRectMake(scrollViewWidth*2, 0,scrollViewWidth, 125))
        imgThree.image = UIImage(named: "bannerImage")
        
        self.scrollView.addSubview(imgOne)
        self.scrollView.addSubview(imgTwo)
        self.scrollView.addSubview(imgThree)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //To update the page control as the banner slides
    func scrollViewDidEndDecelerating(scrollView: UIScrollView){
        // Test the offset and calculate the current page after scrolling ends
        let pageWidth:CGFloat = CGRectGetWidth(scrollView.frame)
        var currentPage:CGFloat = floor((scrollView.contentOffset.x+pageWidth/2)/pageWidth)
        if(currentPage==2) {
            currentPage=0
        }
        else {
            currentPage = currentPage+1
        }
        // Change the indicator
        self.pageControl.currentPage = Int(currentPage);
    }
    
    //Auto slide the banner
    func moveToNextPage (){
        scrollViewDidEndDecelerating(scrollView)
        let pageWidth:CGFloat = CGRectGetWidth(self.scrollView.frame)
        let maxWidth:CGFloat = pageWidth * 3
        let contentOffset:CGFloat = self.scrollView.contentOffset.x
        
        var slideToX = contentOffset + pageWidth
        
        if  contentOffset + pageWidth == maxWidth{
            slideToX = 0
        }
        self.scrollView.scrollRectToVisible(CGRectMake(slideToX, 0, pageWidth, CGRectGetHeight(self.scrollView.frame)), animated: true)
    }
    
    //Get category title according to row no
    /*func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return categories[section]
    }*/
    
    //Get rows according to no of categories
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return dataController.categories.count
    }
    
    //No of rows under one heading
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    //To set each row of table
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        //Create reusable row
        var row:CategoryRow? = tableView.dequeueReusableCellWithIdentifier("cell") as? CategoryRow
        
        //To refresh the value of cell so it does not repeat itself
        row = CategoryRow(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "cell")
        row = tableView.dequeueReusableCellWithIdentifier("cell") as? CategoryRow
        
        //Assign total products in each category according to row
        row?.productCount = countProductsInCategory(indexPath.section)
        row?.productIDList = addToProductList(indexPath.section, productCount: row!.productCount)
        
        //To set header title as cateogy name
        let headerTitle = setHeaderTitle(indexPath.section)
        row!.addSubview(headerTitle)
        
        //To set header button and pass value of category id as parameter
        let headerButton = setHeaderButton(indexPath.section)
        row!.addSubview(headerButton)
        
        return row!
    }
    
    //To set header title as cateogy name
    func setHeaderTitle(catId:Int) -> UILabel {
        let headerTitle = UILabel(frame: CGRectMake(10, 0, 150, 25))
        headerTitle.text = dataController.categories[catId]
        headerTitle.textColor = UIColor(red: 0.6, green: 0.6, blue: 0.6, alpha: 1.0)
        headerTitle.font = UIFont(name: "Arial-BoldMT", size: 16)
        return headerTitle
    }
    
    //To set header button
    func setHeaderButton(rowId:Int) -> UIButton {
        let screenWidth = CGRectGetWidth(productList.frame)
        let headerButton = UIButton(frame: CGRectMake(screenWidth-85, 0, 75, 25))
        headerButton.setTitle("View All", forState: .Normal)
        headerButton.backgroundColor = UIColor(red: 0.6, green: 0.6, blue: 0.6, alpha: 1.0)
        headerButton.titleLabel!.font = UIFont(name: "Arial-BoldMT", size: 14)
        
        //To pass value of category id as parameter in viewAll Function
        headerButton.tag = rowId
        //To add function to the button
        headerButton.addTarget(self, action: "viewAll:", forControlEvents: .TouchUpInside)
        
        return headerButton
    }
    
    //To open the selected category
    func viewAll(sender: UIButton!)
    {
        selectedCategory = sender.tag
        performSegueWithIdentifier("openCategory", sender: self)
    }
    
    //To open the products view
    @IBAction func viewProductsView(sender: AnyObject) {
        selectedProduct = sender.tag
        performSegueWithIdentifier("buyViewToProducts", sender: self)
    }
    
    //To open new views
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        //To open Category View through openCategory segue
        if (segue.identifier == "openCategory") {
            let nav = segue.destinationViewController as! UINavigationController
            let svc = nav.topViewController as! CategoryViewController;
            svc.selectedCategory = selectedCategory
            svc.productCount = countProductsInCategory(selectedCategory)
            svc.productIDList = addToProductList(selectedCategory, productCount: svc.productCount)
        }
        //To open Product View through buyViewToProducts segue
        if (segue.identifier == "buyViewToProducts") {
            print("reached prepareForSegue Function. Selected product is \(selectedProduct) and selected category is \(dataController.productsCategory[selectedProduct])")
            let nav = segue.destinationViewController as! ProductViewController;
            nav.selectedProduct = selectedProduct
            nav.selectedCategory = dataController.productsCategory[selectedProduct]
            nav.previousPage = "buyView"
        }
    }
}
