//
//  CategoryRow.swift
//  Astrology
//
//  Created by Rishabh Wadhwa on 24/11/15.
//  Copyright © 2015 Sunpac Solutions. All rights reserved.
//

import UIKit

class CategoryRow: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var productCount:Int = 0
    var productIDList:[Int]!
    var selectedProduct:Int!
    
    //To get all the product details
    let dataController = DataController.sharedInstance
    
    
    //Set no of products in a category
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return productCount
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("productCell", forIndexPath: indexPath)
        //Create product image holder
        let productImage = UIImageView(frame: CGRectMake(15, 5,110, 110))
        //Add product image to it
        productImage.image = UIImage(named: dataController.productImageName[productIDList[indexPath.row]])
        
        //Create product title
        let productTitle = UILabel(frame: CGRectMake(0, 115, 140, 25))
        productTitle.backgroundColor = UIColor(red: 0.6, green: 0.6, blue: 0.6, alpha: 1.0)
        productTitle.text = (dataController.productName[productIDList[indexPath.row]])
        productTitle.textAlignment = .Center
        productTitle.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1.0)
        productTitle.font = UIFont(name: "Arial-BoldMT", size: 14)
        
        //Combine image and title as a button
        let productButton:UIButton = cell.viewWithTag(7) as! UIButton
        //productButton.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1.0)
        productButton.addSubview(productImage)
        productButton.addSubview(productTitle)
        
        //To pass value of product id as parameter in viewAll Function
        productButton.tag = productIDList[indexPath.row]

        //cell.addSubview(productImage)
        cell.addSubview(productButton)
        return cell
    }
    
    /*override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }*/

}
