//
//  ProblemsViewController.swift
//  Astrology
//
//  Created by Rishabh Wadhwa on 16/11/15.
//  Copyright © 2015 Sunpac Solutions. All rights reserved.
//

import UIKit

class ProblemsViewController: UIViewController {
    
    //To get all the product details
    let dataController = DataController.sharedInstance
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func problemSelected(sender: AnyObject){

        // Create an instance of PlayerTableViewController and pass the variable
        
        print("Button Clicked \(sender)")
        
        self.performSegueWithIdentifier("goToDetailsPage", sender: sender)
        
        
    }

    @IBOutlet var remediesButton: UIButton!
    
    @IBOutlet var careerButton: UIButton!

    @IBOutlet var relationshipButton: UIButton!
    
    @IBOutlet var familyButton: UIButton!
    

    let data = [
        
        "remedies":
        [
            "image":"remidiesProblems",
            "title":"Remedies",
            "text": "Here is full text of Remedies, suggestions for common problems. Lorem Ipsuma asdj alsdj asjdlkja slkdjasda sldjkl asjdkljasd lsdj ads ads."
        ],
        
        "career":
        [
                "image":"careerProblems",
                "title":"Career",
                "text": "Here is full text of Career, suggestions for common problems. Lorem Ipsuma asdj alsdj asjdlkja slkdjasda sldjkl asjdkljasd lsdj ads ads."
        ],
        
        "relationship":
        [
                "image":"relationshipProblems",
                "title":"Relationship",
                "text": "Here is full text of Relationship, suggestions for common problems. Lorem Ipsuma asdj alsdj asjdlkja slkdjasda sldjkl asjdkljasd lsdj ads ads."
        ],
        
        "family":
        [
                "image":"familyProblems",
                "title":"Family",
                "text": "Here is full text of Family, suggestions for common problems. Lorem Ipsuma asdj alsdj asjdlkja slkdjasda sldjkl asjdkljasd lsdj ads ads."
        ]
    ]
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        
        //        let nav = segue.destinationViewController as!
        //        nav.selectedProduct = selectedProduct
        //        nav.selectedCategory = selectedCategory
        //        nav.previousPage = "category"
        
        if(segue.identifier == "goToDetailsPage"){
            print ("Going To Details Page with Selected Item as : ")
            
            let detailsToShow = segue.destinationViewController as! DetailViewController
            
            detailsToShow.cameFrom = 2
            
            switch(sender as! NSObject){
                
            case remediesButton:
                print("Get Remedies")
                
                detailsToShow.mainImageName = data["remedies"]!["image"]!
                detailsToShow.mainNameText = data["remedies"]!["title"]!
                detailsToShow.mainTextText = data["remedies"]!["text"]!
                
                break
            case careerButton:
                print("Get Career")
                detailsToShow.mainImageName = data["career"]!["image"]!
                detailsToShow.mainNameText = data["career"]!["title"]!
                detailsToShow.mainTextText = data["career"]!["text"]!
                break;
                
            case relationshipButton:
                print("Get Relationship")
                detailsToShow.mainImageName = data["relationship"]!["image"]!
                detailsToShow.mainNameText = data["relationship"]!["title"]!
                detailsToShow.mainTextText = data["relationship"]!["text"]!
                
                break;
                
            case familyButton:
                print("Get Family")
                
                detailsToShow.mainImageName = data["family"]!["image"]!
                detailsToShow.mainNameText = data["family"]!["title"]!
                detailsToShow.mainTextText = data["family"]!["text"]!
                
                
                break;
                
            default:
                break;
            }

            
            
        }
        
    }
    
}